
# Description:

This repo contains some console tools, that was writen for last 5 years and some copied from w3crapcli project.


# List of tools:

* speak - utility, that speaking string by google voice.

* trance - utility for trance.fm site, for listening trance music from console. You may select channel [1-5] and baudrate 64, 128, 192, 320.

* loop - run command in loop in infinite cycle.

* 42fm - client for online radio http://42fm.ru/

* fuckinggreatadvice - client for http://fucking-great-advice.ru/

* huificator - online client for http://huifikator.ru/

* lockscreen - console command for lock desktop

* notify - gnome lm-notify wrapper (for remote)

* sendto - send lm-notify message over SSH

* peeep - online client, saving site's snapshots to http://www.peeep.us/

* resolveips - get IPs of host

* serials - show list of your local serial devices

* storages - show list of storage devices, like /dev/sd*

* shortik - online client for http://shortiki.com/

* uurl - unshortener for short URLs. This tool may show to you all chain of redirects (only if no js).

* xfind - find wrapper

* resetusb - Tool for resetting USB device without reconnect

* dropscreen - create screenshot to your dropbox

* vkisonline - checking online/offline status of user for http://vk.com/

## DNS

* opendns - once writes OpenDNS IP to resolv.conf

* googledns - once writes Google DNS IP (8.8.8.8) to resolv.conf


## And some service tools and requirements:

* htmldecode - decode text from html

* urlencode, urldecode - ...

* sendheader - strange parser and sender of saved  HTTP requests


# Special thanks

Special thanks to w3crapcli project! https://github.com/l29ah/w3crapcli

# Contact

For any suggestions, fixes and additions you can contact me via e-mail:
[info@lomalkin.ru](mailto:info@lomalkin.ru?subject=linux%20scripts)
